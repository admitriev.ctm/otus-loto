# используем образ Python3
FROM python:3.8

# TODO спросить зачем буфферизация включается\выключатеся?
ENV PYTHONUNBUFFERED 1

# устанавливаем рабочую директорию
WORKDIR /code

# копируем requirements и устанавливаем зависимости
COPY requirements.txt /code/
RUN pip install -r requirements.txt

# копируем исходный код
COPY ./otusloto/ /code/

# запускаем программу
CMD ["python", "/code/otusloto/main.py"]