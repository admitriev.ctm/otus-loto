import random
from typing import Union


class Bag:
    """
    Мешок для игры в Лото
    """

    def __init__(self, size: int = 90):
        """
        Инициировать объект начальным количеством боченков

        :param size: количество боченков в мешке. По умолчанию 90
        """
        self.__keg_list = [x for x in range(1, size + 1)]

    def get_keg_number(self) -> Union[int, None]:
        """
        Получить номер боченка из мешка

        :return: номер боченка или None, если мешок пуст
        """
        keg_count = len(self.__keg_list)
        if keg_count == 0:
            return None

        index = random.randint(0, keg_count - 1)
        keg_number = self.__keg_list.pop(index)

        return keg_number

    @property
    def length(self) -> int:
        return len(self.__keg_list)
