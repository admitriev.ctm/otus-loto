from .player import Player
from .bag import Bag


class Game:
    """
    Игра Лото
    """

    def __init__(self):
        """
        Инициализация игры
        """
        self.__players = []
        self.__bag = Bag()

    def add_player(self, player: Player):
        """
        Добавление нового игрока

        :param player: игрок - объект класса Player
        """
        self.__players.append(player)

    def add_players(self, players: list):
        """
        Добавление игроков списокм

        :param players: список игроков list(Player)
        """
        for player in players:
            self.add_player(player)

    def start(self):
        """
        Начинает игру

        """
        keep_play = True
        while keep_play:
            keg_number = self.__bag.get_keg_number()
            print(f"\nНовый бочонок: {keg_number} (осталось {self.__bag.length})")
            for player in self.__players:
                player.move(keg_number)
                if player.lose:
                    print(f'Как жаль! "{player}" проиграл!')
                    keep_play = False
                    break
                if player.win:
                    print(f'Поздравляем! "{player}" победил!')
                    keep_play = False
                    break
            if self.__bag.length == 0:
                break
        print('Закончили игру')
