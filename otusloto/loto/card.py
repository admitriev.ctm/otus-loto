from .bag import Bag
import random
from typing import Union


class Card:
    """
    Карточка для игры в Лото
    """

    def __init__(self):
        """
        Инициализирует карточку значениями
        """
        self.__unstriked_count = 15
        self.__numbers = [['' for _ in range(9)] for _ in range(3)]
        self.__fill_numbers()

    def __fill_numbers(self) -> None:
        """
        Заполняет карточку значениями
        """
        all_kegs = Bag()

        for row in self.__numbers:
            all_indexes = [x for x in range(9)]

            # отбираем номера боченков и
            # номера индексов для заполнения строки
            kegs = []
            indexes = []
            for _ in range(5):
                kegs.append(all_kegs.get_keg_number())
                pop_index = random.randint(0, len(all_indexes) - 1)
                indexes.append(all_indexes.pop(pop_index))
            kegs.sort()
            indexes.sort()

            # заполняем строку
            for i, index in enumerate(indexes):
                row[index] = kegs[i]

    def _get_position(self, number: int) -> Union[dict, None]:
        for r, row in enumerate(self.__numbers):
            for c, value in enumerate(row):
                if value == number:
                    return {'row': r, 'column': c}
                if isinstance(value, int):
                    if int(value) > number:
                        break
        return None

    @property
    def unstriked_count(self) -> int:
        """
        Свойство

        :return: количество оставшихся не вычеркнутыми чисел
        """
        return self.__unstriked_count

    def print(self, player_name: str) -> None:
        """
        Выводит на экран карточку
        """
        print(f'--- {player_name} ', end='')
        print('-' * (27 - 5 - len(player_name)))
        for row in self.__numbers:
            for number in row:
                print(f'{number:>3}', end='')
            print('')
        print('-' * 27)

    def strike_out(self, number: int) -> bool:
        """
        Вычеркивает число на карточке.

        :param number: вычеркиваемое число
        :return: True, если число найдено на карточке, False - если не найдено
        """
        position = self._get_position(number)
        if position:
            self.__numbers[position['row']][position['column']] = '-'
            self.__unstriked_count -= 1
            return True
        return False

    def skip(self, number: int) -> bool:
        """
        Пропускает число (подразумевается, что его нет на карточке)

        :param number: пропускаемое число
        :return: True, если число не найдено на карточке, False - если найдено
        """
        position = self._get_position(number)
        if position:
            self.__numbers[position['row']][position['column']] = 'x'
            return False
        return True
