# from card import Card
from .card import Card


class Player:
    """
    Игрок в Лото
    """

    def __init__(self, name: str, is_user: bool = False):
        """
        Инициализация игрока в Лото

        :param name: имя игрока
        :param is_user: True - игрок человек, False - компьютер
        """
        self.__name = name
        self.__is_user = is_user
        self.__lose = False
        self.__win = False
        self.__card = Card()

    def move(self, number: int) -> None:
        """
        Ход игрока

        :param number: номер выпавшего боченка
        """
        self.__card.print(str(self))
        print('Вычеркнуть число? (y/n) ', end='')
        if self.__is_user:
            choice = input('').lower()
            while choice not in ('y', 'n'):
                print('Некорректный ввод, попробуйте еще раз!')
                choice = input('Выберите действие (y - вычеркнуть, n - пропустить): ')

            if choice == 'y':
                if not self.__card.strike_out(number):
                    self.__lose = True

            if not self.__card.skip(number):
                self.__lose = True
        else:
            if self.__card.strike_out(number):
                print('y')
            else:
                print('n')

        if self.__card.unstriked_count == 0:
            self.__win = True

    @property
    def is_user(self) -> bool:
        return self.__is_user

    @property
    def lose(self) -> bool:
        return self.__lose

    @property
    def win(self) -> bool:
        return self.__win

    def __str__(self):
        if self.__is_user:
            player_type = 'игрок'
        else:
            player_type = 'компьютер'
        return f'{self.__name} ({player_type})'
