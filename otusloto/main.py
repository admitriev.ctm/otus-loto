from loto import Game, Player


def play():
    game = Game()
    game.add_player(Player(name="User", is_user=True))
    game.add_player(Player(name="Computer", is_user=False))
    game.start()


if __name__ == '__main__':
    play()
